import time
from os import environ
import redis


# Request redis url from environment variable
if "REDIS_URL" in environ:
    __redis_url__ = environ["REDIS_URL"]
else:
    __redis_url__ = 'localhost'

# Request git HASH from environment variable
if "CI_COMMIT_SHORT_SHA" in environ:
    __version__ = environ["CI_COMMIT_SHORT_SHA"]
else:
    __version__ = "develop.3"


# Create a redis cache
cache = redis.Redis(host=__redis_url__, port=6379)

def get_hits_count():
    """ Get and increment cache on redis """
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)